# Be sure to restart your server when you modify this file.

# Your secret key is used for verifying the integrity of signed cookies.
# If you change this key, all old signed cookies will become invalid!

# Make sure the secret is at least 30 characters and all random,
# no regular words or you'll be exposed to dictionary attacks.
# You can use `rake secret` to generate a secure secret key.

# Make sure your secret_key_base is kept private
# if you're sharing your code publicly.
FirstApp::Application.config.secret_key_base = '3b3fd0298e292956bc46fd6792324bd727d68a9d4e2c65af0182c84803b26f0f4cb5f7eb68bebe71242701b90d9c464cfadb8812c421e4ab7f0ae5d965e5c3dc'
